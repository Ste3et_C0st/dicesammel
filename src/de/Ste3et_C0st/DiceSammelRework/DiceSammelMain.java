package de.Ste3et_C0st.DiceSammelRework;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.Objects;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import com.comphenix.protocol.ProtocolLibrary;

import de.Ste3et_C0st.DiceSammelRework.command.commandExecuter;
import de.Ste3et_C0st.DiceSammelRework.events.SammelListeners;
import de.Ste3et_C0st.DiceSammelRework.utilities.LanguageManager;
import de.Ste3et_C0st.DiceSammelRework.utilities.SammelPlaceholder;


public class DiceSammelMain extends JavaPlugin{

	private static DiceSammelMain instance;
	private final SammelPlayerManager playerManager = new SammelPlayerManager();
	private final SammelObjectManager objectManager = new SammelObjectManager();
	private final LanguageManager languageManager = new LanguageManager();
	private int ThreadTimer = 1000;
	private SammelPlaceholder sammelPlaceholder = null;
	
	public void onEnable() {
		instance = this;
		
		getConfig().addDefaults(YamlConfiguration.loadConfiguration(loadStream("config.yml")));
        getConfig().options().copyDefaults(true);
        getConfig().options().copyHeader(true);
        saveConfig();
        
        this.ThreadTimer = getConfig().getInt("config.threadTime", 1000);
        
        this.languageManager.load("commands");
		this.getCommand("sammel").setExecutor(new commandExecuter());
		this.objectManager.getObjectSet().addAll(SammelObjectID.loadObjectID());
		
		if(Bukkit.getPluginManager().isPluginEnabled("PlaceholderAPI")) {
			this.sammelPlaceholder = new SammelPlaceholder();
			this.sammelPlaceholder.register();
		}
		
		Bukkit.getOnlinePlayers().stream().forEach(player -> this.loadPlayerData(player).updateItems());
		Bukkit.getPluginManager().registerEvents(new SammelListeners(), getInstance());
		Bukkit.getScheduler().runTaskLater(getInstance(), () -> {
			this.objectManager.getObjectSet().forEach(SammelObjectID::initThread);
		}, 10L);
	}
	
	public BufferedReader loadStream(String str) {
        if (!str.startsWith("/"))
            str = "/" + str;
        InputStream stream = getInstance().getClass().getResourceAsStream(str);
        return new BufferedReader(new InputStreamReader(stream, StandardCharsets.UTF_8));
    }
	
	public void onDisable() {
		Bukkit.getOnlinePlayers().stream().forEach(player -> {
			SammelPlayer sammelPlayer = getPlayer(player);
			if(Objects.nonNull(sammelPlayer)) {
				this.objectManager.getObjectSet().forEach(entry -> entry.savePlayerData(sammelPlayer));
				this.objectManager.hideAll(player);
			}
		});
		this.objectManager.getObjectSet().forEach(SammelObjectID::saveObjectID);
		this.objectManager.getObjectSet().forEach(SammelObjectID::stopThread);
		Bukkit.getOnlinePlayers().stream().forEach(this.objectManager::hideAll);
		if(Objects.nonNull(this.sammelPlaceholder)) this.sammelPlaceholder.unregister();
		ProtocolLibrary.getProtocolManager().removePacketListeners(instance);
		instance = null;
	}
	
	public static DiceSammelMain getInstance() {
		return instance;
	}

	public SammelPlayerManager getPlayerManager() {
		return playerManager;
	}

	public SammelObjectManager getObjectManager() {
		return objectManager;
	}

	public LanguageManager getLanguageManager() {
		return languageManager;
	}
	
	public static SammelPlayer getPlayer(OfflinePlayer player) {
		return SammelPlayerManager.cast(player);
	}
	
	public static SammelPlayer getPlayer(String str) {
		return SammelPlayerManager.cast(str);
	}
	
	public int getThreadTime() {
		return this.ThreadTimer;
	}
	
	public SammelPlayer loadPlayerData(Player player) {
		if(Objects.nonNull(player)) {
			if(player.isOnline()) {
				SammelPlayer sammelPlayer = DiceSammelMain.getPlayer(player);
				DiceSammelMain.getInstance().getObjectManager().getObjectSet().stream().forEach(entry -> entry.loadPlayerData(sammelPlayer));
				DiceSammelMain.getInstance().getObjectManager().randomItem(sammelPlayer);
				return sammelPlayer;
			}
		}
		return null;
	}
}

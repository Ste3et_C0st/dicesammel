package de.Ste3et_C0st.DiceSammelRework.events;

import java.util.Objects;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerTeleportEvent;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.events.ListenerPriority;
import com.comphenix.protocol.events.PacketAdapter;
import com.comphenix.protocol.events.PacketEvent;

import de.Ste3et_C0st.DiceSammelRework.DiceSammelMain;
import de.Ste3et_C0st.DiceSammelRework.SammelPlayer;
import de.Ste3et_C0st.DiceSammelRework.SammelPlayerManager;
import de.Ste3et_C0st.DiceSammelRework.utilities.DoubleKey;

public class SammelListeners implements Listener{
	
	@EventHandler
	public void onJoin(PlayerJoinEvent event) {
		if(event.getPlayer().isOnline()) {
			final SammelPlayer sammelPlayer = DiceSammelMain.getInstance().loadPlayerData(event.getPlayer());
			Bukkit.getScheduler().runTaskLater(DiceSammelMain.getInstance(), () -> {
				sammelPlayer.updateItems();
			}, 5l);
		}
	}
	
	@EventHandler
	public void onQuit(PlayerQuitEvent event) {
		SammelPlayer sammelPlayer = DiceSammelMain.getPlayer(event.getPlayer());
		if(Objects.nonNull(sammelPlayer)) {
			DiceSammelMain.getInstance().getObjectManager().getObjectSet().stream().forEach(entry -> entry.savePlayerData(sammelPlayer));
			SammelPlayerManager.getPlayers().remove(sammelPlayer.getUniqueId());
		}
	}
	
	
	@EventHandler
	public void onTeleport(PlayerTeleportEvent event) {
		SammelPlayer sammelPlayer = DiceSammelMain.getPlayer(event.getPlayer());
		if(Objects.nonNull(sammelPlayer)) {
			Bukkit.getScheduler().runTaskLater(DiceSammelMain.getInstance(), () -> {
				DiceSammelMain.getInstance().getObjectManager().update(sammelPlayer);
			}, 5l);
		}
	}
	
	@EventHandler
    public void onPlayerMove(PlayerMoveEvent e) {
        Player player = e.getPlayer();
        SammelPlayer sammelPlayer = DiceSammelMain.getPlayer(player);
        
        if (Objects.isNull(sammelPlayer)) return;
        if (player.getHealth() <= 0.0D) return;
        
        DoubleKey<Integer> oldChunk = new DoubleKey<Integer>(e.getFrom().getBlockX() >> 4, e.getFrom().getBlockZ() >> 4);
        DoubleKey<Integer> newChunk = new DoubleKey<Integer>(e.getTo().getBlockX() >> 4, e.getTo().getBlockZ() >> 4);

        if (!oldChunk.equals(newChunk)) DiceSammelMain.getInstance().getObjectManager().update(sammelPlayer);
    }
	
}

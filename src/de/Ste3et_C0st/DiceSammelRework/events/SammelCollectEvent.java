package de.Ste3et_C0st.DiceSammelRework.events;

import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import de.Ste3et_C0st.DiceSammelRework.SammelItem;
import de.Ste3et_C0st.DiceSammelRework.SammelObjectID;
import de.Ste3et_C0st.DiceSammelRework.SammelPlayer;

public class SammelCollectEvent extends Event implements Cancellable{

	private static final HandlerList handlers = new HandlerList();
	private final SammelItem sammelItem;
	 private final SammelObjectID objectID;
	private final SammelPlayer sammelPlayer;
    private String message;
    private boolean cancelled;
    
    
    public SammelCollectEvent(SammelItem sammelItem, SammelPlayer sammelPlayer, SammelObjectID objectID) {
        this.sammelItem = sammelItem;
        this.sammelPlayer = sammelPlayer;
        this.objectID = objectID;
    }

    public String getMessage() {
        return message;
    }

    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

	@Override
	public boolean isCancelled() {
		return cancelled;
	}

	@Override
	public void setCancelled(boolean arg0) {
		this.cancelled = arg0;
	}

	public SammelItem getSammelItem() {
		return sammelItem;
	}

	public SammelPlayer getSammelPlayer() {
		return sammelPlayer;
	}

	public SammelObjectID getObjectID() {
		return objectID;
	}
	
	
}

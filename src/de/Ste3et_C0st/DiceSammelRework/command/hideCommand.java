package de.Ste3et_C0st.DiceSammelRework.command;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.Ste3et_C0st.DiceSammelRework.DiceSammelMain;

public class hideCommand extends commandHandler{

	public hideCommand(String mainArgument) {
		super(mainArgument);
	}

	@Override
	public void execute(CommandSender sender, String[] arg3) {
		DiceSammelMain.getInstance().getObjectManager().hideAll(Player.class.cast(sender));
	}

}

package de.Ste3et_C0st.DiceSammelRework.command;

import java.util.Objects;

import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import de.Ste3et_C0st.DiceSammelRework.DiceSammelMain;
import de.Ste3et_C0st.DiceSammelRework.SammelItem;
import de.Ste3et_C0st.DiceSammelRework.SammelObjectID;
import de.Ste3et_C0st.DiceSammelRework.SammelSelector;
import de.Ste3et_C0st.DiceSammelRework.utilities.StringTranslater;

public class addCommand extends commandHandler{

	public addCommand(String mainArgument) {
		super(mainArgument);
	}
	
	@Override
	public void execute(CommandSender sender, String[] args) {
		if(Player.class.isInstance(sender)) {
			Player player = Player.class.cast(sender);
			SammelSelector selector = DiceSammelMain.getInstance().getObjectManager().getSelector(player);
			if(Objects.isNull(selector)) {
				sender.sendMessage(getCommandMessage("base.noCategorySelected"));
			}else {
				SammelObjectID objectID = selector.getObjectID();
				ItemStack stack = player.getInventory().getItemInMainHand();
				if(Objects.isNull(stack) || stack.getType().equals(Material.AIR)) {
					sender.sendMessage(getCommandMessage(getLanguage() + ".missingItem"));
				}else {
					SammelItem sammelItem = new SammelItem(stack, player.getLocation().add(0, .7, 0), objectID);
					objectID.sammelObjects.add(sammelItem);
					sammelItem.send(player);
					sender.sendMessage(getCommandMessage(getLanguage() + ".success",
						new StringTranslater("%ITEM%", stack.getType().name().toLowerCase()),
						new StringTranslater("%CATEGORY%", objectID.getObjectName())
					));
				}
			}
		}
	}

}

package de.Ste3et_C0st.DiceSammelRework.command;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.Ste3et_C0st.DiceSammelRework.DiceSammelMain;
import de.Ste3et_C0st.DiceSammelRework.SammelPlayer;
import de.Ste3et_C0st.DiceSammelRework.SammelSelector;
import de.Ste3et_C0st.DiceSammelRework.utilities.StringTranslater;

public class finishCommand extends commandHandler{

	public finishCommand(String mainArgument) {
		super(mainArgument);
	}

	@Override
	public void execute(CommandSender sender, String[] args) {
		if(Player.class.isInstance(sender)) {
			Player player = Player.class.cast(sender);
			SammelPlayer sammelPlayer = DiceSammelMain.getPlayer(player);
			if(sammelPlayer.isEditor()) {
				SammelSelector selector = sammelPlayer.getSelector();
				DiceSammelMain.getInstance().getObjectManager().getSammelSelector().remove(selector);
				sender.sendMessage(getCommandMessage("command.finish.success", new StringTranslater("%CATEGORY%", selector.getObjectID().getObjectName())));
			}else {
				sender.sendMessage(getCommandMessage("base.noCategorySelected"));
			}
		}	
	}
	
	

}

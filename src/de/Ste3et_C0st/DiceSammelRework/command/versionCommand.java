package de.Ste3et_C0st.DiceSammelRework.command;

import org.bukkit.command.CommandSender;

import de.Ste3et_C0st.DiceSammelRework.DiceSammelMain;
import de.Ste3et_C0st.DiceSammelRework.utilities.StringTranslater;

public class versionCommand extends commandHandler{

	public versionCommand(String mainArgument) {
		super(mainArgument);
	}

	@Override
	public void execute(CommandSender sender, String[] arg3) {
		sender.sendMessage(getCommandMessage(
				new StringTranslater("%PLUGIN_NAME%", DiceSammelMain.getInstance().getName()),
				new StringTranslater("%PLUGIN_VERSION%", DiceSammelMain.getInstance().getDescription().getVersion())
		));
	}

}

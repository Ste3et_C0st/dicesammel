package de.Ste3et_C0st.DiceSammelRework.command;

import java.util.HashSet;
import java.util.Objects;
import java.util.stream.Stream;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import de.Ste3et_C0st.DiceSammelRework.DiceSammelMain;
import de.Ste3et_C0st.DiceSammelRework.SammelItem;
import de.Ste3et_C0st.DiceSammelRework.SammelObjectID;
import de.Ste3et_C0st.DiceSammelRework.SammelPlayer;
import de.Ste3et_C0st.DiceSammelRework.utilities.StringTranslater;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;

public class listCommand extends commandHandler{

	public listCommand(String mainArgument) {
		super(mainArgument);
	}

	@Override
	public void execute(CommandSender sender, String[] args) {
		if(Player.class.isInstance(sender)) {
			Player player = Player.class.cast(sender);
			SammelPlayer sammelPlayer = DiceSammelMain.getPlayer(player);
			if(!sammelPlayer.isEditor()) {
				if(DiceSammelMain.getInstance().getObjectManager().getObjectSet().isEmpty()) {
					sender.sendMessage(getCommandMessage(getLanguage() + ".fail.noCategory"));
					return;
				}else {
					int page = 0;
					if(args.length > 1) {
						try {
							page = Integer.parseInt(args[1]);
						}catch (Exception e) {
							e.printStackTrace();
						}
					}
					sendCategorys(sender, page);
				}
			}else {
				SammelObjectID objectID = sammelPlayer.getSelector().getObjectID();
				SammelPlayer otherPlayer = null;
				int page = 0;
				if(args.length > 1) {
					try {
						page = Integer.parseInt(args[1]);
					}catch (Exception e) {}
					if(args.length == 3) {
						Player onlinePlayer = Bukkit.getPlayer(args[2]);
						if(Objects.nonNull(onlinePlayer)) {
							otherPlayer = DiceSammelMain.getPlayer(onlinePlayer);
						}
					}
				}
				
				if(objectID.getSammelSet().isEmpty()) {
					sender.sendMessage(getCommandMessage(getLanguage() + ".fail.noItems", new StringTranslater("%CATEGORY%", objectID.getObjectName())));
					return;
				}else {
					sendItems(sender, page, objectID, Objects.isNull(otherPlayer) ? sammelPlayer : otherPlayer);
				}
			}
		}
	}
	
	public void sendCategorys(CommandSender sender, int page) {
		sender.sendMessage(getCommandMessage(getLanguage() + ".header", new StringTranslater("%LIST_TYPE%", "Kategorien")));
		HashSet<SammelObjectID> sammelSet = DiceSammelMain.getInstance().getObjectManager().getObjectSet();
		int objects = 10;
		double d = Math.ceil(sammelSet.size() / ((double) objects));
		int maxSide = (int) d;
		
		sammelSet.stream().skip(page*objects).limit(objects).forEach(entry -> {
			ComponentBuilder componentBuilder = new ComponentBuilder("§7 - §9" + entry.getObjectName() + " ");
			componentBuilder.append("§2<< ")
				.event(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("§9Selektiere Kategorie: §e" + entry.getObjectName()).create()))
				.event(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/sammel select " + entry.getObjectName()));
			componentBuilder.append("§3X")
				.event(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("§9Entfernt Kategorie: §c" + entry.getObjectName()).create()))
				.event(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/sammel remove " + entry.getObjectName()));
			sender.spigot().sendMessage(componentBuilder.create());
		});
		
		if(maxSide > 1) {
			ComponentBuilder builder = new ComponentBuilder(getCommandMessage(getLanguage() + ".prevCommand"))
								.color(page < 1 ? ChatColor.GRAY : ChatColor.YELLOW).event(new ClickEvent(ClickEvent.Action.RUN_COMMAND, page < 1 ? "" : "/sammel list " + (page +1 )))
								.append("                                                    ")
								.append(getCommandMessage(getLanguage() + ".nextCommand")).color((page + 1) < maxSide ? ChatColor.GREEN : ChatColor.GRAY)
																						  .event(new ClickEvent(ClickEvent.Action.RUN_COMMAND, (page + 1) < maxSide ? "" : "/sammel list " + (page - 1)));
			sender.spigot().sendMessage(builder.create());
		}
		
		ComponentBuilder componentBuilder = new ComponentBuilder(getCommandMessage(getLanguage() + ".footer", 
																	new StringTranslater("%SIDE%", (page + 1) + ""), 
																	new StringTranslater("%MAX_SIDE%", maxSide + ""))
																);
		sender.spigot().sendMessage(componentBuilder.create());
	}
	
	public void sendItems(CommandSender sender, int page, SammelObjectID objectID, SammelPlayer sammelPlayer) {
		sender.sendMessage(getCommandMessage(getLanguage() + ".header", new StringTranslater("%LIST_TYPE%", "Items: " + objectID.getObjectName())));
		int objects = 15;
		double d = Math.ceil(objectID.getSammelSet().size() / ((double) objects));
		int maxSide = (int) d;
		Stream<SammelItem> sammelStream = objectID.getSammelSet().stream();
		sammelStream = sammelStream.sorted((k1, k2) -> Boolean.compare(sammelPlayer.canCollect(k1, objectID), sammelPlayer.canCollect(k2, objectID)));
		
		if(sammelPlayer.getOfflinePlayer().getName().equalsIgnoreCase(sender.getName())) {
			sammelStream = sammelStream.sorted((k1,k2) -> Integer.compare(k2.getEntityID(), k1.getEntityID()));
		}
		
		sammelStream.skip(page * objects).limit(objects).forEach(entry -> {
			ComponentBuilder componentBuilder = new ComponentBuilder("§7 - " + (sammelPlayer.canCollect(entry, objectID) ? "§2" : "§c") + entry.getItemStackUUID().toString() + " ");
			componentBuilder.event(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(
					"§7Item: §a" + entry.getItemStack().getType().name() + "\n" +
					"§7Respawned: " + (sammelPlayer.canCollect(entry, objectID) ? "§2ja" : "§cnein -> " + objectID.respawnText(sammelPlayer.getCollectedItem(entry.getItemStackUUID()).getCollectedTime()))
			).create()));
			componentBuilder.append("§2<< ")
				.event(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("§9Telepotiert dich zum Item").create()))
				.event(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/sammel teleport " + entry.getItemStackUUID().toString()));
			componentBuilder.append("§3X")
				.event(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("§9Entfernt das Item aus der Kategorie").create()))
				.event(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/sammel remove " + entry.getItemStackUUID().toString()));
			sender.spigot().sendMessage(componentBuilder.create());
		});
		
		if(maxSide > 1) {
			ComponentBuilder prevBuilder = new ComponentBuilder(getCommandMessage(getLanguage() + ".prevCommand"));
			
			if(page > 0) {
				prevBuilder.color(ChatColor.YELLOW);
				prevBuilder.event(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/sammel list " + (page -1 ) + " " + sammelPlayer.getOfflinePlayer().getName()));
			}else {
				
			}
			
			prevBuilder.append("                                                    ");
			
			if((page + 1) < maxSide) {
				prevBuilder.append(getCommandMessage(getLanguage() + ".nextCommand"));
				prevBuilder.color(ChatColor.GREEN);
				prevBuilder.event(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/sammel list " + (page + 1) + " " + sammelPlayer.getOfflinePlayer().getName()));
			}else {
				prevBuilder.append(getCommandMessage(getLanguage() + ".nextCommand"));
				prevBuilder.reset();
				prevBuilder.color(ChatColor.GRAY);
			}
			
			
			sender.spigot().sendMessage(prevBuilder.create());
		}
		
		ComponentBuilder componentBuilder = new ComponentBuilder(getCommandMessage(getLanguage() + ".footer", 
				new StringTranslater("%SIDE%", (page + 1) + ""), 
				new StringTranslater("%MAX_SIDE%", maxSide + ""))
			);
		sender.spigot().sendMessage(componentBuilder.create());
	}
}

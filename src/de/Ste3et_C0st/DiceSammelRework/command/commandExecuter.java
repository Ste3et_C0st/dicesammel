package de.Ste3et_C0st.DiceSammelRework.command;

import java.util.HashSet;
import java.util.Objects;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import de.Ste3et_C0st.DiceSammelRework.utilities.LanguageManager;

public class commandExecuter implements CommandExecutor{

	private HashSet<commandHandler> commandSet = new HashSet<commandHandler>();
	
	public commandExecuter() {
		commandSet.add(new versionCommand("version"));
		commandSet.add(new createCommand("create"));
		commandSet.add(new addCommand("add"));
		commandSet.add(new reloadCommand("reload"));
		commandSet.add(new finishCommand("finish"));
		commandSet.add(new removeCommand("remove"));
		commandSet.add(new listCommand("list"));
		commandSet.add(new selectCommand("select"));
		commandSet.add(new teleportCommand("teleport"));
		commandSet.add(new resetCommand("reset"));
		commandSet.add(new hideCommand("hide"));
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command command, String str, String[] args) {
		if(command.getName().equalsIgnoreCase("sammel")) {
			if(!sender.hasPermission("sammel.help")) {
				sender.sendMessage(LanguageManager.getInstance().getString("base.noPermissions"));
				return true;
			}
			if(args.length == 0) {
				sender.sendMessage(LanguageManager.getInstance().getString("command.main"));
			}else {
				String arg = args[0].toLowerCase();
				commandHandler handler = this.commandSet.stream().filter(cmd -> cmd.getMainArgument().equalsIgnoreCase(arg)).findFirst().orElse(null);
				if(Objects.isNull(handler)) {
					sender.sendMessage(LanguageManager.getInstance().getString("command.main"));
					return true;
				}else {
					if(!sender.hasPermission(handler.getPermission())) {
						sender.sendMessage(LanguageManager.getInstance().getString("base.noPermissions"));
						return true;
					}
					handler.execute(sender, args);
				}
			}
			return true;
		}
		return false;
	}
	
	public HashSet<commandHandler> getCommandHandler() {
		return commandSet;
	}

}

package de.Ste3et_C0st.DiceSammelRework.command;

import java.util.Objects;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.Ste3et_C0st.DiceSammelRework.DiceSammelMain;
import de.Ste3et_C0st.DiceSammelRework.SammelItem;
import de.Ste3et_C0st.DiceSammelRework.SammelPlayer;

public class teleportCommand extends commandHandler{

	public teleportCommand(String mainArgument) {
		super(mainArgument);
	}

	@Override
	public void execute(CommandSender sender, String[] args) {
		if(args.length == 2) {
			if(Player.class.isInstance(sender)) {
				Player player = Player.class.cast(sender);
				SammelPlayer sammelPlayer = DiceSammelMain.getPlayer(Player.class.cast(sender));
				if(sammelPlayer.isEditor()) {
					SammelItem item = sammelPlayer.getSelector().getObjectID().getSammelSet().stream().filter(entry -> entry.getItemStackUUID().toString().equalsIgnoreCase(args[1])).findFirst().orElse(null);
					if(Objects.nonNull(item)) {
						player.teleport(item.getLocation());
					}
				}
			}
		}
	}
}

package de.Ste3et_C0st.DiceSammelRework.command;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.Ste3et_C0st.DiceSammelRework.DiceSammelMain;
import de.Ste3et_C0st.DiceSammelRework.SammelObjectID;
import de.Ste3et_C0st.DiceSammelRework.SammelSelector;
import de.Ste3et_C0st.DiceSammelRework.utilities.StringTranslater;

public class createCommand extends commandHandler{

	public createCommand(String mainArgument) {
		super(mainArgument);
	}

	@Override
	public void execute(CommandSender sender, String[] args) {
		if(args.length == 2) {
			String category = args[1];
			if(DiceSammelMain.getInstance().getObjectManager().getObjectSet().stream().filter(entry -> entry.getObjectName().equalsIgnoreCase(category)).findFirst().isPresent()) {
				sender.sendMessage(getCommandMessage(getLanguage() + ".alreadyExist", new StringTranslater("%CATEGORY%", category)));
				return;
			}else{
				if(Player.class.isInstance(sender)) {
					Player player = Player.class.cast(sender);
					if(DiceSammelMain.getInstance().getObjectManager().getSammelSelector().stream().filter(entry -> entry.getPlayer().equals(player)).findFirst().isPresent()) {
						sender.sendMessage(getCommandMessage("base.alreadyInSelectMode"));;
					}else {
						SammelObjectID sammelCategory = new SammelObjectID(player.getWorld(), category);
						DiceSammelMain.getInstance().getObjectManager().getObjectSet().add(sammelCategory);
						DiceSammelMain.getInstance().getObjectManager().getSammelSelector().add(new SammelSelector(player, sammelCategory));
						sender.sendMessage(getCommandMessage(getLanguage() + ".success", new StringTranslater("%CATEGORY%", category)));
					}
					return;
				}
			}
		}else {
			sender.sendMessage(getCommandMessage(getLanguage() + ".missingArgument"));
		}
	}

}

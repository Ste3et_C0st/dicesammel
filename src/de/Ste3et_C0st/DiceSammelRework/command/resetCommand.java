package de.Ste3et_C0st.DiceSammelRework.command;

import java.io.File;
import java.util.Objects;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.Ste3et_C0st.DiceSammelRework.CollectedItem;
import de.Ste3et_C0st.DiceSammelRework.DiceSammelMain;
import de.Ste3et_C0st.DiceSammelRework.SammelObjectID;
import de.Ste3et_C0st.DiceSammelRework.SammelPlayer;
import de.Ste3et_C0st.DiceSammelRework.SammelPlayerManager;

public class resetCommand extends commandHandler{

	public resetCommand(String mainArgument) {
		super(mainArgument);
	}

	@Override
	public void execute(CommandSender sender, String[] args) {
		if(Player.class.isInstance(sender)) {
			Player player = Player.class.cast(sender);
			SammelPlayer sammelPlayer = DiceSammelMain.getPlayer(player);
			if(sammelPlayer.isEditor()) {
				SammelObjectID objectID = sammelPlayer.getSelector().getObjectID();
				if(Objects.nonNull(objectID)) {
					objectID.getSammelSet().stream().forEach(entry -> {
						SammelPlayerManager.getPlayers().values().stream().forEach(entryPlayer -> {
							CollectedItem item = entryPlayer.getCollectedItem(entry.getItemStackUUID());
							entryPlayer.getCollectedUUIDs().remove(item);
						});
						entry.destroyPlayer(Bukkit.getOnlinePlayers());
					});
					objectID.sammelObjects.clear();
					new File(objectID.getFolder(), "playerData").deleteOnExit();
					objectID.saveObjectID();
				}
			}
		}
	}
	
}

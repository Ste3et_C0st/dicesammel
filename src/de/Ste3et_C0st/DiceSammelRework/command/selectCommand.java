package de.Ste3et_C0st.DiceSammelRework.command;

import java.util.Objects;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.Ste3et_C0st.DiceSammelRework.DiceSammelMain;
import de.Ste3et_C0st.DiceSammelRework.SammelObjectID;
import de.Ste3et_C0st.DiceSammelRework.SammelPlayer;
import de.Ste3et_C0st.DiceSammelRework.utilities.StringTranslater;

public class selectCommand extends commandHandler{

	public selectCommand(String mainArgument) {
		super(mainArgument);
	}
	
	@Override
	public void execute(CommandSender sender, String[] args) {
		if(Player.class.isInstance(sender)) {
			Player player = Player.class.cast(sender);
			SammelPlayer sammelPlayer = DiceSammelMain.getPlayer(player);
			if(sammelPlayer.isEditor()) {
				sender.sendMessage(getCommandMessage("base.alreadyInSelectMode"));
				return;
			}else {
				if(args.length == 2) {
					SammelObjectID objectID = DiceSammelMain.getInstance().getObjectManager().getSammelObject(args[1]);
					if(Objects.nonNull(objectID)) {
						sammelPlayer.joinEditorMode(objectID);
						sender.sendMessage(getCommandMessage(getLanguage() + ".success", new StringTranslater("%CATEGORY%", objectID.getObjectName())));
					}else {
						sender.sendMessage(getCommandMessage("base.notExist"));
					}
				}else {
					sender.sendMessage(getCommandMessage(getLanguage() + ".fail.missingArgument"));
				}
			}
		}
	}
}

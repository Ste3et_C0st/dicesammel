package de.Ste3et_C0st.DiceSammelRework.command;

import org.bukkit.command.CommandSender;

import de.Ste3et_C0st.DiceSammelRework.utilities.LanguageManager;
import de.Ste3et_C0st.DiceSammelRework.utilities.StringTranslater;

public abstract class commandHandler {

	private final String mainArgument, permission;
	
	public commandHandler(String mainArgument) {
		this.mainArgument = mainArgument;
		this.permission = "sammel.command." + mainArgument.toLowerCase();
	}

	public String getMainArgument() {
		return mainArgument;
	}

	public String getPermission() {
		return permission;
	}
	
	public String getLanguage() {
		return "command." + mainArgument.toLowerCase();
	}
	
	public boolean hasPermission(CommandSender sender) {
		return sender.isOp() ? true : sender.hasPermission(getPermission());
	}
	
	public LanguageManager getLanguageManager() {
		return LanguageManager.getInstance();
	}
	
	public String getCommandMessage(StringTranslater ... translater) {
		return this.getCommandMessage(getLanguage(), translater);
	}
	
	public String getCommandMessage(String key, StringTranslater ... translater) {
		return getLanguageManager().getString(key, translater);
	}
	
	public abstract void execute(CommandSender sender, String[] arg3);
}

package de.Ste3et_C0st.DiceSammelRework.command;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.Ste3et_C0st.DiceSammelRework.DiceSammelMain;
import de.Ste3et_C0st.DiceSammelRework.SammelObjectID;

public class reloadCommand extends commandHandler{

	public reloadCommand(String mainArgument) {
		super(mainArgument);
	}

	@Override
	public void execute(CommandSender sender, String[] args) {
		if(Player.class.isInstance(sender)) {
			DiceSammelMain.getInstance().getLanguageManager().reload();
			DiceSammelMain.getInstance().getObjectManager().getObjectSet().stream().forEach(SammelObjectID::initPickupHandler);
			sender.sendMessage(DiceSammelMain.getInstance().getLanguageManager().getString("command.reload"));
		}
	}

}

package de.Ste3et_C0st.DiceSammelRework.command;

import java.util.Objects;
import java.util.UUID;

import org.bukkit.Location;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.Ste3et_C0st.DiceSammelRework.DiceSammelMain;
import de.Ste3et_C0st.DiceSammelRework.SammelItem;
import de.Ste3et_C0st.DiceSammelRework.SammelObjectID;
import de.Ste3et_C0st.DiceSammelRework.SammelPlayer;
import de.Ste3et_C0st.DiceSammelRework.SammelSelector;
import de.Ste3et_C0st.DiceSammelRework.utilities.StringTranslater;

public class removeCommand extends commandHandler{

	public removeCommand(String mainArgument) {
		super(mainArgument);
	}

	@Override
	public void execute(CommandSender sender, String[] args) {
		if(Player.class.isInstance(sender)) {
			Player player = Player.class.cast(sender);
			SammelPlayer sammelPlayer = DiceSammelMain.getPlayer(player);
			if(sammelPlayer.isEditor()) {
				SammelSelector selector = sammelPlayer.getSelector();
				Location location = player.getLocation();
				if(args.length == 1) {
					SammelItem item = selector.getObjectID().getSammelSet().stream().filter(sammel -> sammel.getLocation().distance(location) < 1).findFirst().orElse(null);
					if(Objects.nonNull(item)) {
						selector.getObjectID().getSammelSet().remove(item);
						item.destroy(player);
						sender.sendMessage(getCommandMessage(getLanguage() + ".success.distance", 
								new StringTranslater("%ITEM%", item.getItemStack().getType().name().toLowerCase()),
								new StringTranslater("%CATEGORY%", selector.getObjectID().getObjectName())
						));
					}else {
						sender.sendMessage(getCommandMessage(getLanguage() + ".fail.distance"));
					}
				}else if(args.length == 2) {
					try {
						UUID uuid = UUID.fromString(args[1]);
						SammelItem item = selector.getObjectID().getSammelSet().stream().filter(sammel -> sammel.getItemStackUUID().equals(uuid)).findFirst().orElse(null);
						if(Objects.nonNull(item)) {
							selector.getObjectID().getSammelSet().remove(item);
							item.destroy(player);
							sender.sendMessage(getCommandMessage(getLanguage() + ".success.uuid", 
									new StringTranslater("%ITEM%", item.getItemStack().getType().name().toLowerCase()),
									new StringTranslater("%CATEGORY%", selector.getObjectID().getObjectName())
							));
						}
					}catch (Exception e) {
						sender.sendMessage(getCommandMessage(getLanguage() + ".fail.uuid"));
					}
				}else {
					sender.sendMessage(getCommandMessage(getLanguage() + ".missingArgument"));
				}
			}else {
				if(args.length == 2) {
					String category = args[1];
					SammelObjectID objectID = DiceSammelMain.getInstance().getObjectManager().getSammelObject(category);
					if(Objects.nonNull(objectID)) {
						sender.sendMessage(getCommandMessage(getLanguage() + ".success.category", new StringTranslater("%CATEGORY%", objectID.getObjectName())));
						DiceSammelMain.getInstance().getObjectManager().getObjectSet().remove(objectID);
						return;
					}else {
						sender.sendMessage(getCommandMessage("base.notExist"));
					}
				}else {
					
					sender.sendMessage(getCommandMessage("base.noCategorySelected"));
				}
			}
		}
	}

}

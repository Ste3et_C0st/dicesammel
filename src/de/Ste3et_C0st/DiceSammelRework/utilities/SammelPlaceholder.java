package de.Ste3et_C0st.DiceSammelRework.utilities;

import java.util.Objects;

import org.bukkit.OfflinePlayer;
import de.Ste3et_C0st.DiceSammelRework.DiceSammelMain;
import de.Ste3et_C0st.DiceSammelRework.SammelObjectID;
import de.Ste3et_C0st.DiceSammelRework.SammelPlayer;
import me.clip.placeholderapi.expansion.PlaceholderExpansion;

public class SammelPlaceholder extends PlaceholderExpansion{

	@Override
	public String getAuthor() {
		return "Ste3et_C0st";
	}

	@Override
	public String getIdentifier() {
		return "sammel";
	}

	@Override
	public String getVersion() {
		return "1.1";
	}
	
	public String onRequest(OfflinePlayer player, String identifier) {
		//sammel
		if(identifier.toLowerCase().startsWith("size_")) {
			String tag = identifier.toLowerCase().replace("size_", "");
			SammelObjectID objectID = DiceSammelMain.getInstance().getObjectManager().getSammelObject(tag);
			if(Objects.nonNull(objectID)) {
				return objectID.sammelObjects.size() + "";
			}
		}else if(identifier.toLowerCase().startsWith("count_")) {
			String tag = identifier.toLowerCase().replace("count_", "");
			SammelObjectID objectID = DiceSammelMain.getInstance().getObjectManager().getSammelObject(tag);
			if(Objects.nonNull(objectID)) {
				SammelPlayer sammelPlayer = DiceSammelMain.getPlayer(player.getName());
				if(Objects.nonNull(sammelPlayer)) {
					return objectID.getAmountReal(sammelPlayer) + "";
				}
			}
		}else if(identifier.toLowerCase().startsWith("timer_")) {
			String tag = identifier.toLowerCase().replace("timer_", "");
			SammelObjectID objectID = DiceSammelMain.getInstance().getObjectManager().getSammelObject(tag);
			if(Objects.nonNull(objectID)) {
				SammelPlayer sammelPlayer = DiceSammelMain.getPlayer(player.getName());
				if(Objects.nonNull(sammelPlayer)) {
					return sammelPlayer.getNextRespawnString(objectID) + "";
				}
			}
		}
		return identifier;
	}
}

package de.Ste3et_C0st.DiceSammelRework.utilities;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import org.bukkit.configuration.file.YamlConfiguration;

import de.Ste3et_C0st.DiceSammelRework.DiceSammelMain;
import net.md_5.bungee.api.ChatColor;

public class LanguageManager {

	private HashMap<String, String> language = new HashMap<String, String>();
	private HashSet<String> fileNames = new HashSet<String>();
	private static LanguageManager instance;
	public LanguageManager() {
		instance = this;
	}
	
	public void load(String a) {
		File folder = new File("plugins/" + DiceSammelMain.getInstance().getName() + "/language");
		if(!folder.exists()) folder.mkdirs();
		InputStreamReader isReader = null;
		InputStream reader = null;
		YamlConfiguration config = YamlConfiguration.loadConfiguration(new File(folder, a + ".yml"));
		if(!this.fileNames.contains(a)) this.fileNames.add(a);
		try {
			reader = DiceSammelMain.getInstance().getResource("language/"+a+".yml");
			isReader = new InputStreamReader(reader);
			YamlConfiguration defaultConfig = YamlConfiguration.loadConfiguration(isReader);
			config.addDefaults(defaultConfig);
		}catch (Exception e) {}
		config.options().copyDefaults(true);
		config.options().copyHeader(true);
		try {
			config.save(new File(folder, a + ".yml"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		config.getConfigurationSection("language").getKeys(true).stream().forEach(str -> {
			String key = "language." + str;
			if(config.isString(key)) {
				String value = config.getString(key);
				language.put(str.toLowerCase(), value);
			}else if(config.isList(key)) {
				String value = "";
				List<String> stringList = config.getStringList(key);
				int end = (stringList.size() - 1);
				for(String s : stringList) {
					if(stringList.indexOf(s) != end) {
						value+=s+"\n";
					}else {
						value+=s;
					}
				}
				language.put(str.toLowerCase(), value);
			}else {
				language.put(str.toLowerCase(), str);
			}
		});
		try {
			if(isReader != null) isReader.close();
			if(reader != null) reader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void reload() {
		this.language.clear();
		this.fileNames.stream().forEach(this::load);
	}
	
	public static LanguageManager getInstance(){
		return instance;
	}
	
	public String getStringRaw(String key) {
		key = key.toLowerCase();
		String value = key;
		if(language.containsKey(key)) value = language.get(key);
		return value;
	}
	
	public String getString(String key) {
		return ChatColor.translateAlternateColorCodes('&', getStringRaw(key));
	}
	
	public String getString(String key, StringTranslater ... stringTranslaters) {
		String a = getString(key);
		if(stringTranslaters != null) {
			for(StringTranslater trans : stringTranslaters) {
				if(trans.getkey() != null && trans.getValue() != null) {
					a = a.replaceAll(trans.getkey(), trans.getValue());
				}
			}
		}
		return a;
	}
}

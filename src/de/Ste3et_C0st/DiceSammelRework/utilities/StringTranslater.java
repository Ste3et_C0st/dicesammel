package de.Ste3et_C0st.DiceSammelRework.utilities;

public class StringTranslater {

	private String key, value;
	public StringTranslater(String key, String value) {
		this.key = key;
		this.value = value;
	}
	
	public String getkey() {
		return this.key;
	}
	
	public String getValue() {
		return this.value;
	}
	
}

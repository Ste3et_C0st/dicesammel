package de.Ste3et_C0st.DiceSammelRework;

<<<<<<< HEAD
import org.bukkit.Bukkit;

import java.lang.reflect.Field;
import java.util.concurrent.atomic.AtomicInteger;

public class EntityID {

	private static Class<?> entityClass;
	private static Field entityCountField;
	public static String version;
	
	static {
		String name = Bukkit.getServer().getClass().getPackage().getName();
		version = name.substring(name.lastIndexOf('.') + 1) + ".";
		try {
			entityClass = Class.forName(getVersionInt() == 17 ? "net.minecraft.world.entity.Entity" : "net.minecraft.server." + getVersion() + "Entity");
			entityCountField = entityClass.getDeclaredField(getVersionInt() == 17 ? "b" : "entityCount");
			entityCountField.setAccessible(true);
		}catch(Exception ex) {
			ex.printStackTrace();
		}
	}
	
    public static int nextEntityIdOld() {
        try {
            int id = entityCountField.getInt(null);
            entityCountField.set(null, id + 1);
            return id;
        } catch (Exception e) {
        	System.out.println(e.getMessage());
            return 0;
        }
    }

    private static int getVersionInt() {
		return 17;
	}

	public static int nextEntityIdNew() {
        try {
			return AtomicInteger.class.cast(entityCountField.get(null)).incrementAndGet();
        } catch (Exception e) {
        	System.out.println(e.getMessage());
            return 0;
        }
    }

    public static int nextEntityId() {
        if (getVersionInt() > 13) {
            return nextEntityIdNew();
        } else {
            return nextEntityIdOld();
        }
    }

    public static Class<?> getNMSClass(String className) {
        String fullName = "net.minecraft.server." + getVersion() + className;
        Class<?> clazz = null;
        try {
            clazz = Class.forName(fullName);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return clazz;
    }

    public static String getVersion() {
		return version;
    }
=======
import java.lang.reflect.Field;

import org.bukkit.Bukkit;

public class EntityID {

	public static String version = "1." + getBukkitVersion().split("_")[1];
	
	private static int nextEntityIdOld(){
		try{
			Class<?> entityClass = Class.forName("net.minecraft.server." + getVersion() + "Entity");
			Field f = entityClass.getDeclaredField("entityCount");
			f.setAccessible(true);
			int id = f.getInt(null);
			f.set(null, id+1);
			return id;
		}catch(Exception e){
			e.printStackTrace();
			return 0;
		}
	}
	
	private static int nextEntityIdNew(){
		try {
			Class<?> entityClass = Class.forName("net.minecraft.server." + getVersion() + "Entity");
			Field f = entityClass.getDeclaredField("entityCount"); 
			f.setAccessible(true);
			Object obj = f.get(null);
			int id = (int) obj.getClass().getMethod("incrementAndGet").invoke(obj);
			return id;
		}catch (Exception e) {
			e.printStackTrace();
			return 0;
		}
	}
	
	public static int nextEntityId() {
		if(version.equalsIgnoreCase("1.14") || version.equalsIgnoreCase("1.15") || version.equalsIgnoreCase("1.16")) {
			return nextEntityIdNew();
		}else {
			return nextEntityIdOld();
		}
	}
	
	public static Class<?> getNMSClass(String className) {
		String fullName = "net.minecraft.server." + getVersion() + className;
		Class<?> clazz = null;
		try{
			clazz = Class.forName(fullName);
		}catch (Exception e){
			e.printStackTrace();
		}
		return clazz;
	}

	public static String getBukkitVersion() {
		return Bukkit.getServer().getClass().getPackage().getName().replace(".", ",").split(",")[3];
	}
	
	public static String getVersion() {
		String name = Bukkit.getServer().getClass().getPackage().getName();
		String version = name.substring(name.lastIndexOf('.') + 1) + ".";
		return version;
	}
>>>>>>> branch 'master' of https://gitlab.com/Ste3et_C0st/dicesammel.git
}

package de.Ste3et_C0st.DiceSammelRework;

import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.stream.Collectors;

import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

public class SammelPlayer {

	private final OfflinePlayer player;
	private List<CollectedItem> uuidSet = new CopyOnWriteArrayList<CollectedItem>();
	private HashSet<SammelItem> sendetItem = new HashSet<SammelItem>();
	private CollectedItem latestCollectedItem = null;
	
	private final UUID uuid;
	
	public SammelPlayer(OfflinePlayer player) {
		this.player = player;
		this.uuid = player.getUniqueId();
		SammelPlayerManager.getPlayers().put(this.uuid, this);
	}

	public OfflinePlayer getOfflinePlayer() {
		return player;
	}
	
	public List<CollectedItem> getCollectedUUIDs(){
		return this.uuidSet;
	}
	
	public CollectedItem getCollectedItem(UUID uuid) {
		return this.uuidSet.stream().filter(entry -> entry.getUUID().equals(uuid)).findFirst().orElse(null);
	}
	
	public boolean hasCollected(UUID uuid) {
		return uuidSet.stream().filter(entry -> entry.getUUID().equals(uuid)).findFirst().isPresent();
	}
	
	public boolean canCollect(SammelItem item, SammelObjectID objectID) {
		if(Objects.isNull(item)) return false;
		CollectedItem collectedItem = getCollectedItem(item.getItemStackUUID());
		if(Objects.nonNull(collectedItem)) {
			return objectID.canRespawnTime(collectedItem.getCollectedTime());
		}
		return true;
	}
	
	public boolean isEditor() {
		return Objects.nonNull(this.getSelector());
	}
	
	public SammelSelector getSelector() {
		if(!this.getOfflinePlayer().isOnline()) return null;
		Player player = this.getOfflinePlayer().getPlayer();
		return DiceSammelMain.getInstance().getObjectManager().getSelector(player);
	}
	
	public SammelSelector joinEditorMode(SammelObjectID objectID) {
		if(!this.getOfflinePlayer().isOnline()) return null;
		Player player = this.getOfflinePlayer().getPlayer();
		SammelSelector selector = new SammelSelector(player, objectID);
		DiceSammelMain.getInstance().getObjectManager().getSammelSelector().add(selector);
		return selector;
	}
	
	public boolean isOnline() {
		return this.getOfflinePlayer().isOnline();
	}
	
	public Player getPlayer() {
		if(this.isOnline()) {
			return this.getOfflinePlayer().getPlayer();
		}
		return null;
	}

	public UUID getUniqueId() {
		return uuid;
	}
	
	public void setLatestCollectedItem(CollectedItem collectedItem) {
		this.latestCollectedItem = collectedItem;
	}
	
	public CollectedItem getLatestCollectedItem() {
		return this.latestCollectedItem;
	}
	
	public String getNextRespawnString(SammelObjectID sammelObjectID) {
		if(this.getCollectedUUIDs().size() == 0) return "jetzt";
		if(this.latestCollectedItem == null) {
			Optional<CollectedItem> timeStamp = getNextRespawn(sammelObjectID);
			if(timeStamp.isPresent()) {
				latestCollectedItem = timeStamp.get();
				return timeStamp.get().respawnText();
			}
		}else {
			return latestCollectedItem.respawnText();
		}
		return "jetzt";
	}
	
	public Optional<CollectedItem> getNextRespawn(SammelObjectID sammelObjectID) {
		if(sammelObjectID.getRespawnTime() > 0L) {
			List<SammelItem> item = sammelObjectID.getCollectedItems(this);
			if(item.isEmpty()) return Optional.empty();
			List<CollectedItem> collectedItem = item.stream().map(entry -> this.getCollectedItem(entry.getItemStackUUID())).collect(Collectors.toList());
			if(collectedItem.isEmpty() == false) {
				Optional<CollectedItem> latest = collectedItem.stream().sorted((k1,k2) -> Long.compare(k1.getCollectedTime(), k2.getCollectedTime())).findFirst();
				if(latest.isPresent()) {
					return latest;
				}
			}
		}
		return Optional.empty();
	}
	
	public HashSet<SammelItem> getReceivetSammelItemSet(){
		return this.sendetItem;
	}

	public void updateItems() {
		DiceSammelMain.getInstance().getObjectManager().update(this);
	}

	public boolean hasAlreadySend(SammelItem sammelItem) {
		return this.sendetItem.contains(sammelItem);
	}
}

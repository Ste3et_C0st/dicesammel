package de.Ste3et_C0st.DiceSammelRework;

import java.util.UUID;

public class CollectedItem {

	private UUID uuid;
	private long pickupTime = -1L;
	private final SammelItem sammelItem;
	
	public CollectedItem(UUID uuid, SammelItem sammelItem) {
		this(uuid, System.currentTimeMillis(), sammelItem);
	}
	
	public CollectedItem(UUID uuid, long pickupTime, SammelItem sammelItem) {
		this.pickupTime = pickupTime;
		this.uuid = uuid;
		this.sammelItem = sammelItem;
	}
	
	public UUID getUUID() {
		return this.uuid;
	}
	
	public long getCollectedTime() {
		return this.pickupTime;
	}
	
	public SammelItem getSammelItem() {
		return this.sammelItem;
	}
	
	public String respawnText() {
		long pickUptime = this.sammelItem.getSammelObjectID().calculateRespawn(this.pickupTime);
		if(pickUptime == -1L) return "Niemals";
		pickUptime = getSammelItem().getSammelObjectID().getRespawnTime() - pickUptime;
		if(pickUptime < 0) return "Jetzt";
		long seconds = pickUptime / 1000;
		long s = seconds % 60;
	    long m = (seconds / 60) % 60;
	    long h = (seconds / (60 * 60));
	    return String.format("%d:%02d:%02d", h,m,s);
	}
}

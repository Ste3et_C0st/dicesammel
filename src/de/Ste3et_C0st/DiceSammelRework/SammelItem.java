package de.Ste3et_C0st.DiceSammelRework;

import java.util.Collection;
import java.util.UUID;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import de.Ste3et_C0st.DiceSammelRework.utilities.DoubleKey;

public class SammelItem{

	private ItemDropPacket itemStackPacket;
	private final ArmorStandPacket armorstandPacket;
	private final Location location,itemstackLocation;
	private final SammelObjectID sammelObjectID;
	private final int chunkX, chunkZ;
	private int entityID;
	private final UUID itemStackUUID;
	private DoubleKey<Integer> chunkKey;
	
	public SammelItem(ItemStack stack, Location location, SammelObjectID sammelObjectID) {
		this(stack, location, UUID.randomUUID(), sammelObjectID);
	}
	
	public SammelItem(ItemStack stack, Location location, UUID uuid, SammelObjectID sammelObjectID) {
		this.location = location;
		this.chunkX = location.getBlockX() >> 4;
		this.chunkZ = location.getBlockZ() >> 4;
		this.sammelObjectID = sammelObjectID;
		this.chunkKey = new DoubleKey<Integer>(chunkX, chunkZ);
		this.itemstackLocation = this.location.clone().add(0, .2, 0);
		this.itemStackUUID = uuid;
		this.armorstandPacket = new ArmorStandPacket(location.clone().subtract(0, 1.7, 0));
		this.armorstandPacket.setInvisible(true);
		this.setItemStack(stack);
	}
	
	public void setItemStack(ItemStack stack) {
		this.itemStackPacket = new ItemDropPacket(stack, location, getItemStackUUID());
		this.entityID = this.itemStackPacket.getEntityID();
		this.armorstandPacket.getPassenger().add(this.itemStackPacket.getEntityID());
	}
	
	public void send(Player player) {
		this.sendToPlayer(player);
	}
	
	public void destroy(Player player) {
		this.destroyPlayer(player);
	}
	
	public void sendToPlayer(Player ... players) {
		this.armorstandPacket.send(players);
		this.itemStackPacket.send(players);
		this.armorstandPacket.sendMount(players);
		this.itemStackPacket.sendMetadata(players);
	}
	
	public void destroyPlayer(Player ... players) {
		this.armorstandPacket.destroy(players);
		this.itemStackPacket.destroy(players);
	}

	public Location getLocation() {
		return this.location;
	}
	
	public Location getItemStackLocation() {
		return this.itemstackLocation;
	}

	public int getEntityID() {
		return entityID;
	}

	public UUID getItemStackUUID() {
		return itemStackUUID;
	}

	public ItemStack getItemStack() {
		return this.itemStackPacket.getStack();
	}

	public void destroyPlayer(Collection<? extends Player> onlinePlayers) {
		this.destroyPlayer(onlinePlayers.toArray(new Player[onlinePlayers.size()]));
	}
	
	public int compareTo() {
		
		return 0;
	}
	
	public DoubleKey<Integer> getChunkKey(){
		return chunkKey;
	}

	public boolean isInChunk(int chunkX, int chunkZ) {
		return chunkX == this.chunkX && chunkZ == this.chunkZ;
	}

	public SammelObjectID getSammelObjectID() {
		return sammelObjectID;
	}
	
}

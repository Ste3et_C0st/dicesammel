package de.Ste3et_C0st.DiceSammelRework;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;

import com.mojang.datafixers.types.Type.Continue;

import de.Ste3et_C0st.DiceSammelRework.utilities.DoubleKey;

public class SammelObjectManager {

	private HashSet<SammelObjectID> objectSet = new HashSet<SammelObjectID>();
	private HashSet<SammelSelector> sammelSelector = new HashSet<SammelSelector>(); 
	private static int viewRange = 10;
	
	public HashSet<SammelObjectID> getObjectSet() {
		return objectSet;
	}

	public HashSet<SammelSelector> getSammelSelector() {
		return sammelSelector;
	}
	
	public SammelSelector getSelector(Player player) {
		return sammelSelector.stream().filter(entry -> entry.getPlayer().equals(player)).findFirst().orElse(null);
	}
	
	public boolean isInSelector(Player player) {
		return Objects.nonNull(getSelector(player));
	}
	
	public SammelObjectID getSammelObject(String name) {
		return this.objectSet.stream().filter(obj -> obj.getObjectName().equalsIgnoreCase(name)).findFirst().orElse(null);
	}
	
	public void hideAll(Player player) {
		SammelPlayer sammelPlayer = DiceSammelMain.getPlayer(player);
		if(Objects.isNull(player)) return;
		this.hideAll(sammelPlayer);
	}
	
	public void hideAll(SammelPlayer player) {
		this.objectSet.stream().forEach(entry -> {
			entry.sammelObjects.stream().forEach(item -> item.destroy(player.getPlayer()));
			player.getReceivetSammelItemSet().clear();
		});
	}
	
	public Set<DoubleKey<Integer>> viewDistance(Player player){
		World world = player.getWorld();
		int radius = world.getViewDistance();
		Location location = player.getLocation();
		int startChunkX = location.getBlockX() >> 4, startChunkZ = location.getBlockZ() >> 4;
		
		int length = (radius * 2) + 1;
	    Set<DoubleKey<Integer>> chunks = new HashSet<DoubleKey<Integer>>(length * length);

	    for (int x = -radius; x <= radius; x++) {
	        for (int z = -radius; z <= radius; z++) {
	        	chunks.add(new DoubleKey<Integer>(startChunkX + x, startChunkZ + z));
	        }
	    }
	    chunks.add(new DoubleKey<Integer>(startChunkX, startChunkZ));
	    return chunks;
	}
	
	public boolean isInRange(Player player, DoubleKey<Integer> chunkKey) {
<<<<<<< HEAD
    	if(Objects.isNull(player)) return false;
		Location location = player.getLocation();
        return distanceSquared(location, chunkKey) <= viewRange;
    }
    
    private int distanceSquared(Location location, DoubleKey<Integer> chunkKey) {
    	int x = location.getBlockX() >> 4;
        int z = location.getBlockZ() >> 4;
    	return distanceSquared(x, z, chunkKey);
    }
    
    private int distanceSquared(int chunkX, int chunkZ, DoubleKey<Integer> chunkKey) {
    	return square(chunkKey.getKey1() - chunkX) + square(chunkKey.getKey2() - chunkZ);
    }
    
    private static int square(int num) {
        return num * num;
    }
	
	public void randomItem(SammelPlayer player) {
		List<SammelItem> itemList = new ArrayList<SammelItem>();
		this.objectSet.stream().filter(entry -> Objects.nonNull(entry.getPickupHandler())).forEach(sammelObject -> {
			if(sammelObject.getPickupHandler().isNavigation()) {
				itemList.addAll(sammelObject.sammelObjects);
			}
		});
		
		if(!itemList.isEmpty()) {
			Collections.shuffle(itemList);
			SammelItem item = itemList.stream()
					.filter(entry -> !player.hasCollected(entry.getItemStackUUID()))
					.findFirst().orElse(null);
			
			if(Objects.nonNull(item)) {
				player.getPlayer().setCompassTarget(item.getLocation());
			}
		}
	}

	public void update(SammelPlayer sammelPlayer) {
		if(Objects.isNull(sammelPlayer)) return;
		if(Objects.nonNull(sammelPlayer.getPlayer())) {
			World world = sammelPlayer.getPlayer().getWorld();
			Player player = sammelPlayer.getPlayer();
			HashSet<SammelItem> removeItem = new HashSet<SammelItem>(sammelPlayer.getReceivetSammelItemSet());
			HashSet<SammelItem> sendetItem = new HashSet<SammelItem>();
			
			this.objectSet.stream().forEach(entry -> {
				if(entry.getWorld().equals(world)) {
					getAllSammelItemsInView(sammelPlayer, entry, entry.isHideCollected()).stream().forEach(item -> {
						if(removeItem.contains(item)) removeItem.remove(item);
						if(sammelPlayer.hasAlreadySend(item) == false) {
							item.send(player);
							sendetItem.add(item);
						}
					});
				}
			});
			
			removeItem.stream().forEach(entry -> entry.destroy(player));
			sammelPlayer.getReceivetSammelItemSet().addAll(sendetItem);
			sammelPlayer.getReceivetSammelItemSet().removeAll(removeItem);
		}
	}
	
	public HashSet<SammelItem> getAllSammelItemsInView(SammelPlayer sammelPlayer, SammelObjectID sammelObjectID){
		return getAllSammelItemsInView(sammelPlayer, sammelObjectID, true);
	}
	
	public HashSet<SammelItem> getAllSammelItemsInView(SammelPlayer sammelPlayer, SammelObjectID sammelObjectID, boolean hideCollected){
		HashSet<SammelItem> returnSet = new HashSet<SammelItem>();
		if(Objects.nonNull(sammelPlayer)) {
			Player player = sammelPlayer.getPlayer();
			if(Objects.isNull(player)) return returnSet;
			if(player.isOnline() == false) return returnSet;
=======
    	Location location = player.getLocation();
        return distanceSquared(location, chunkKey) <= viewRange;
    }
    
    private int distanceSquared(Location location, DoubleKey<Integer> chunkKey) {
    	int x = location.getBlockX() >> 4;
        int z = location.getBlockZ() >> 4;
    	return distanceSquared(x, z, chunkKey);
    }
    
    private int distanceSquared(int chunkX, int chunkZ, DoubleKey<Integer> chunkKey) {
    	return square(chunkKey.getKey1() - chunkX) + square(chunkKey.getKey2() - chunkZ);
    }
    
    private static int square(int num) {
        return num * num;
    }
	
	public void randomItem(SammelPlayer player) {
		List<SammelItem> itemList = new ArrayList<SammelItem>();
		this.objectSet.stream().filter(entry -> Objects.nonNull(entry.getPickupHandler())).forEach(sammelObject -> {
			if(sammelObject.getPickupHandler().isNavigation()) {
				itemList.addAll(sammelObject.sammelObjects);
			}
		});
		
		if(!itemList.isEmpty()) {
			Collections.shuffle(itemList);
			SammelItem item = itemList.stream()
					.filter(entry -> !player.hasCollected(entry.getItemStackUUID()))
					.findFirst().orElse(null);
			
			if(Objects.nonNull(item)) {
				player.getPlayer().setCompassTarget(item.getLocation());
			}
		}
	}

	public void update(SammelPlayer sammelPlayer) {
		if(Objects.isNull(sammelPlayer)) return;
		if(Objects.nonNull(sammelPlayer.getPlayer())) {
			World world = sammelPlayer.getPlayer().getWorld();
			Player player = sammelPlayer.getPlayer();
			HashSet<SammelItem> removeItem = new HashSet<SammelItem>(sammelPlayer.getReceivetSammelItemSet());
			HashSet<SammelItem> sendetItem = new HashSet<SammelItem>();
			
			this.objectSet.stream().forEach(entry -> {
				if(entry.getWorld().equals(world)) {
					getAllSammelItemsInView(sammelPlayer, entry, entry.isHideCollected()).stream().forEach(item -> {
						if(removeItem.contains(item)) removeItem.remove(item);
						if(sammelPlayer.hasAlreadySend(item) == false) {
							item.send(player);
							sendetItem.add(item);
						}
					});
				}
			});
			
			removeItem.stream().forEach(entry -> entry.destroy(player));
			sammelPlayer.getReceivetSammelItemSet().addAll(sendetItem);
			sammelPlayer.getReceivetSammelItemSet().removeAll(removeItem);
		}
	}
	
	public HashSet<SammelItem> getAllSammelItemsInView(SammelPlayer sammelPlayer, SammelObjectID sammelObjectID){
		return getAllSammelItemsInView(sammelPlayer, sammelObjectID, true);
	}
	
	public HashSet<SammelItem> getAllSammelItemsInView(SammelPlayer sammelPlayer, SammelObjectID sammelObjectID, boolean hideCollected){
		HashSet<SammelItem> returnSet = new HashSet<SammelItem>();
		if(Objects.nonNull(sammelPlayer)) {
			Player player = sammelPlayer.getPlayer();
>>>>>>> branch 'master' of https://gitlab.com/Ste3et_C0st/dicesammel.git
			sammelObjectID.getSammelSet().stream().forEach(entry -> {
				if(isInRange(player, entry.getChunkKey())) {
					CollectedItem collectedItem = sammelPlayer.getCollectedItem(entry.getItemStackUUID());
					if(sammelPlayer.canCollect(entry, sammelObjectID) == true) {
						if(Objects.nonNull(collectedItem)) {
							sammelPlayer.getCollectedUUIDs().remove(collectedItem);
						}
						returnSet.add(entry);
					}else if(hideCollected == false) {
						returnSet.add(entry);
					}
				}
			});
		}
		return returnSet;
	}

}

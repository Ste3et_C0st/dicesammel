package de.Ste3et_C0st.DiceSammelRework;

import java.util.HashMap;
import java.util.Objects;
import java.util.UUID;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;

public class SammelPlayerManager {

	private static HashMap<UUID,SammelPlayer> playerSet = new HashMap<UUID,SammelPlayer>();
	
	public static SammelPlayer cast(OfflinePlayer player) {
		if(Objects.isNull(player)) return null;
		if(playerSet.containsKey(player.getUniqueId())) {
			return playerSet.get(player.getUniqueId());
		}
		return new SammelPlayer(player);
	}
	
	public static SammelPlayer cast(String str) {
		OfflinePlayer offlinePlayer = Bukkit.getOfflinePlayerIfCached(str);
		if(Objects.isNull(offlinePlayer)) return null;
		return cast(offlinePlayer);
	}
	
	public static HashMap<UUID, SammelPlayer> getPlayers(){
		return playerSet;
	}
}

package de.Ste3et_C0st.DiceSammelRework.thread;

import de.Ste3et_C0st.DiceSammelRework.SammelPlayer;
import de.Ste3et_C0st.DiceSammelRework.events.SammelCollectEvent;

public interface SammelPickupHandler {

	public boolean isHideCollected();
	public boolean isNavigation();
	public void onPickup(SammelPlayer player, SammelCollectEvent collectEvent);
	public long getRespawnTime();
	
}

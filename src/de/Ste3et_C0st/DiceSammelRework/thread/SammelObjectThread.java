package de.Ste3et_C0st.DiceSammelRework.thread;

import java.util.Objects;

public class SammelObjectThread implements Runnable {

	private boolean doStop = false;
	private final long time;
	private final SammelWorker worker;
	private Thread thread = null;
	
	public SammelObjectThread(long time, SammelWorker worker) {
		this.time = time;
		if(Objects.nonNull(worker)) {
			this.worker = worker;
			this.thread = new Thread(this);
			this.thread.start();
		}else {
			this.worker = null;
			this.doStop();
		}
	}
	
    public synchronized void doStop() {
        this.doStop = true;
    }

    private synchronized boolean keepRunning() {
        return this.doStop == false;
    }
	
	@Override
	public void run() {
		while(keepRunning()) {
				this.worker.run();
			try {
				Thread.sleep(this.time);
			}catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
}

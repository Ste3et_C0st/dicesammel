package de.Ste3et_C0st.DiceSammelRework.thread;

import java.util.Objects;

import org.bukkit.Bukkit;
import org.bukkit.scheduler.BukkitTask;

import de.Ste3et_C0st.DiceSammelRework.DiceSammelMain;

public class SammelObjectBukkitTask {

	private final long time;
	private final SammelWorker worker;
	private BukkitTask task = null;
	
	public SammelObjectBukkitTask(long time, SammelWorker worker) {
		this.time = time;
		if(Objects.nonNull(worker)) {
			this.worker = worker;
			this.task = Bukkit.getScheduler().runTaskTimerAsynchronously(DiceSammelMain.getInstance(), () -> {
				try {
					this.worker.run();
				}catch (Exception e) {
					e.printStackTrace();
				}
			}, 0, this.time);
		}else {
			this.worker = null;
			
		}
	}
	
	public synchronized void doStop() {
		if(Objects.nonNull(task)) {
			task.cancel();
		}
    }
	
	public boolean isCancelled() {
		return Objects.nonNull(task) ? task.isCancelled() : true;
	}
	
}

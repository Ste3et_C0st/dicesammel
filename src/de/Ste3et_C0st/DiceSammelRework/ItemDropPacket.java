package de.Ste3et_C0st.DiceSammelRework;

import java.util.Arrays;
import java.util.Collection;
import java.util.UUID;

import org.bukkit.Location;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.ProtocolManager;
import com.comphenix.protocol.events.PacketContainer;
import com.comphenix.protocol.wrappers.WrappedDataWatcher;
import com.comphenix.protocol.wrappers.WrappedDataWatcher.Registry;
import com.comphenix.protocol.wrappers.WrappedDataWatcher.WrappedDataWatcherObject;

public class ItemDropPacket {

	private final ItemStack stack;
	private final Location location;
	private PacketContainer container;
	private WrappedDataWatcher watcher;
	private final Integer entityID = EntityID.nextEntityId();
	private final UUID uuid;
	private static ProtocolManager protocolManager = ProtocolLibrary.getProtocolManager();
	
	public ItemDropPacket(ItemStack stack, Location location) {
		this(stack, location, UUID.randomUUID());
	}
	
	public ItemDropPacket(ItemStack stack, Location location, UUID uuid) {
		this.uuid = uuid;
		this.stack = stack;
		this.container = new PacketContainer(PacketType.Play.Server.SPAWN_ENTITY);
		this.location = location;
		
		getHandle().getModifier().writeDefaults();
		getHandle().getEntityTypeModifier().write(0, EntityType.DROPPED_ITEM);
		getHandle().getIntegers().write(0, this.entityID);
		getHandle().getUUIDs().write(0, uuid);
		//getHandle().getIntegers().write(4, 1);
		
		this.setLocation(location);
		this.initDataWatcher();
	}

	public ItemStack getStack() {
		return stack.clone();
	}

	public Location getLocation() {
		return location;
	}
	
	public Integer getEntityID() {
		return this.entityID;
	}

	public PacketContainer getHandle() {
		return container;
	}
	
	public UUID getUUID() {
		return this.uuid;
	}
	
	public void send(final Collection<Player> playerCollection) {
		playerCollection.stream().forEach(this::send);
	}
	
	public void send(final Player ... players) {
		Arrays.asList(players).stream().forEach(this::send);
	}
	
	public void destroy(final Collection<Player> playerCollection) {
		playerCollection.stream().forEach(this::destroy);
	}
	
	public void destroy(final Player ... players) {
		Arrays.asList(players).stream().forEach(this::destroy);
	}
	
	private void setLocation(final Location location) {
		getHandle().getDoubles().write(0, location.getX()).write(1, location.getY() + .2).write(2, location.getZ());
//		byte yaw = ((byte) (int) (location.getYaw() * 256.0F / 360.0F));
//		byte pitch = ((byte) (int) (location.getPitch() * 256.0F / 360.0F));
		//getHandle().getIntegers().write(2, (int) yaw).write(3, (int) pitch);
	}
	
	public void send(final Player player) {
		try {
			protocolManager.sendServerPacket(player, this.getHandle());
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void sendMetadata(final Player ... players) {
		Arrays.asList(players).stream().forEach(this::sendMetadata);
	}
	
	public void sendMetadata(final Player player) {
		try {
			protocolManager.sendServerPacket(player, this.initMetadata());
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void destroy(final Player player) {
		PacketContainer destroy = new PacketContainer(PacketType.Play.Server.ENTITY_DESTROY);
<<<<<<< HEAD
		destroy.getIntegerArrays().writeSafely(0, new int [] {this.entityID});
		destroy.getIntegers().writeSafely(0, this.entityID);
		destroy.getIntLists().writeSafely(0, Arrays.asList(this.entityID));
		try {
			protocolManager.sendServerPacket(player, destroy);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void initDataWatcher() {
		this.watcher = new WrappedDataWatcher();
		this.watcher.setObject(new WrappedDataWatcherObject(8, WrappedDataWatcher.Registry.getItemStackSerializer(false)), getStack());
=======
		destroy.getIntegerArrays().write(0, new int [] {this.entityID});
		try {
			protocolManager.sendServerPacket(player, destroy);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void initDataWatcher() {
		this.watcher = new WrappedDataWatcher();
		this.watcher.setObject(new WrappedDataWatcherObject(7, WrappedDataWatcher.Registry.getItemStackSerializer(false)), getStack());
>>>>>>> branch 'master' of https://gitlab.com/Ste3et_C0st/dicesammel.git
		this.watcher.setObject(new WrappedDataWatcherObject(1, Registry.get(Integer.class)), 300);
		this.watcher.setObject(new WrappedDataWatcherObject(0, Registry.get(Byte.class)), (byte) 0);
		this.watcher.setObject(new WrappedDataWatcherObject(5, Registry.get(Boolean.class)), false);
	}
	
	private PacketContainer initMetadata(){
		PacketContainer metadata = new PacketContainer(PacketType.Play.Server.ENTITY_METADATA);
		metadata.getIntegers().write(0, this.entityID);
		metadata.getWatchableCollectionModifier().write(0, this.watcher.getWatchableObjects());
		return metadata;
	}
	
}

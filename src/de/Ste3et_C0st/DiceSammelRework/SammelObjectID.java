package de.Ste3et_C0st.DiceSammelRework;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import java.util.stream.Collectors;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import de.Ste3et_C0st.DiceSammelRework.events.SammelCollectEvent;
import de.Ste3et_C0st.DiceSammelRework.reward.FileCollector;
import de.Ste3et_C0st.DiceSammelRework.thread.SammelObjectBukkitTask;
import de.Ste3et_C0st.DiceSammelRework.thread.SammelPickupHandler;
import de.Ste3et_C0st.DiceSammelRework.utilities.DoubleKey;

public class SammelObjectID {

	public HashSet<SammelItem> sammelObjects = new HashSet<SammelItem>();
	private SammelObjectBukkitTask objectThread;
	private final World world;
	private final String objectName;
	private SammelPickupHandler pickupHandler = null;
	
	public SammelObjectID(World world, String name) {
		this.world = world;
		this.objectName = name;
		this.initPickupHandler();
	}
	
	public void initThread() {
		this.objectThread = new SammelObjectBukkitTask(DiceSammelMain.getInstance().getThreadTime() * 1L, () ->  {
			HashSet<Player> playerList = new HashSet<Player>(getWorld().getPlayers());
			for (Player player : playerList) {
				Location location = player.getLocation();
				SammelPlayer sammelPlayer = SammelPlayerManager.cast(player);
				HashSet<SammelItem> itemHashSet = DiceSammelMain.getInstance().getObjectManager().getAllSammelItemsInView(sammelPlayer, this, true);
				itemHashSet.stream().filter(entry -> location.distance(entry.getItemStackLocation()) <= 2).forEach(entry -> {
					if(sammelPlayer.hasCollected(entry.getItemStackUUID()) == false) {
						if(sammelPlayer.isEditor() == false) {
							if(sammelPlayer.hasAlreadySend(entry) == true) {
								SammelObjectID.this.collect(sammelPlayer, entry, SammelObjectID.this);
							}
						}
					}
				});
			}
		});
	}
	
	public File getFolder() {
		return new File("plugins/" + DiceSammelMain.getInstance().getName() + "/objects/" + objectName);
	}
	
	public void initPickupHandler() {
		this.pickupHandler = new FileCollector(new File(getFolder(),"pickupConfig.yml"));
	}
	
	public World getWorld() {
		return world;
	}
	
	public HashSet<SammelItem> getSammelSet(){
		return this.sammelObjects;
	}
	
	public int getAmount(SammelPlayer player) {
		int i = 0;
		if(Objects.nonNull(player)) {
			i = (int) getSammelSet().stream().filter(entry -> player.hasCollected(entry.getItemStackUUID())).count();
		}
		return i;
	}

	public String getObjectName() {
		return objectName;
	}
	
	public void stopThread() {
		if(Objects.nonNull(this.objectThread)) {
			this.objectThread.doStop();
		}
	}
	
	public void setPickupHandler(SammelPickupHandler pickupHandler) {
		this.pickupHandler = pickupHandler;
	}
	
	public void collect(SammelPlayer sammelPlayer, SammelItem sammelItem, SammelObjectID objectID) {
		Bukkit.getScheduler().runTask(DiceSammelMain.getInstance(), () -> {
			SammelCollectEvent collectEvent = new SammelCollectEvent(sammelItem, sammelPlayer, objectID);
			Bukkit.getPluginManager().callEvent(collectEvent);
			if(!collectEvent.isCancelled()) {
				if(Objects.nonNull(pickupHandler)) {
					pickupHandler.onPickup(sammelPlayer, collectEvent);
				}
			}
		});
	}
	
	public SammelItem getItem(UUID uuid) {
		return this.sammelObjects.stream().filter(entry -> entry.getItemStackUUID().equals(uuid)).findFirst().orElse(null);
	}
	
	public void saveObjectID() {
		if(!getFolder().exists()) getFolder().mkdirs();
		File file = new File(getFolder(), "SammelObject.yml");
		YamlConfiguration config = new YamlConfiguration();
		
		config.set("object.name", this.getObjectName());
		config.set("object.world", this.getWorld().getName());
		this.sammelObjects.stream().forEach(entry -> {
			config.set("object.items." + entry.getItemStackUUID() + ".location", entry.getLocation());
			config.set("object.items." + entry.getItemStackUUID() + ".itemstack", entry.getItemStack());
		});
		
		try {
			config.save(file);
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		SammelPlayerManager.getPlayers().values().forEach(entry -> {
			this.savePlayerData(entry);
		});
	}
	
	public static List<SammelObjectID> loadObjectID() {
		List<SammelObjectID> objectIds = new ArrayList<SammelObjectID>();
		File folder = new File("plugins/" + DiceSammelMain.getInstance().getName() + "/objects");
		if(folder.exists()) {
			for(File objects : folder.listFiles()) {
				File objectFile = new File(objects, "SammelObject.yml");
				if(objectFile.exists()) {
					YamlConfiguration config = YamlConfiguration.loadConfiguration(objectFile);
					String name = config.getString("object.name");
					World world = Bukkit.getWorld(config.getString("object.world"));
					SammelObjectID objectID = new SammelObjectID(world, name);
					if(config.isConfigurationSection("object.items")) {
						ItemStack fallback = new ItemStack(Material.STONE);
						
						for(String entry : config.getConfigurationSection("object.items").getKeys(false)) {
							UUID uuid = UUID.fromString(entry);
							ItemStack stack = config.getItemStack("object.items." + entry + ".itemstack");
							Location location = config.getLocation("object.items." + entry + ".location");
							objectID.getSammelSet().add(new SammelItem(stack, location, uuid, objectID));
							if(Material.AIR != stack.getType()) {
								fallback = stack;
							}
						}
						
						for(SammelItem sammelItem : objectID.getSammelSet()) {
							if(Material.AIR == sammelItem.getItemStack().getType()) {
								sammelItem.setItemStack(fallback);
							}
						}
					}
					objectIds.add(objectID);
				}
			}
		}
		return objectIds;
	}
	
	public boolean isHideCollected() {
		return this.pickupHandler.isHideCollected();
	}
	
	public void loadPlayerData(SammelPlayer sammelPlayer) {
		File playerFolder = new File(getFolder(), "playerData");
		long l = this.getRespawnTime();
		if(playerFolder.exists()) {
			File playerFile = new File(playerFolder, sammelPlayer.getUniqueId().toString() + ".yml");
			if(playerFile.exists()) {
				YamlConfiguration config = YamlConfiguration.loadConfiguration(playerFile);
				if(config.isConfigurationSection("data")) {
					config.getConfigurationSection("data").getKeys(false).forEach(key -> {
						UUID uuid = UUID.fromString(key);
						SammelItem sammelItem = this.getItem(uuid);
						if(Objects.nonNull(sammelItem)) {
							long collectedTime = l > 0 ? config.getLong("data." + key + ".collectedTime", -1L) : -1L;
							if(this.getSammelSet().stream().filter(entry -> entry.getItemStackUUID().equals(uuid)).findFirst().isPresent()) {
								if(collectedTime == -1L) {
									sammelPlayer.getCollectedUUIDs().add(new CollectedItem(uuid, collectedTime, sammelItem));
								}else if(!canRespawnTime(collectedTime)) {
									sammelPlayer.getCollectedUUIDs().add(new CollectedItem(uuid, collectedTime, sammelItem));
								}
							}
						}
					});
				}
			}
		}
	}
	
	public void savePlayerData(SammelPlayer sammelPlayer) {
		if(Objects.isNull(sammelPlayer)) return;
		File playerFolder = new File(getFolder(), "playerData");
		if(!playerFolder.exists()) playerFolder.mkdirs();
		YamlConfiguration config = new YamlConfiguration();
		sammelPlayer.getCollectedUUIDs().stream().forEach(entry -> {
			config.set("data." + entry.getUUID().toString() + ".collectedTime", entry.getCollectedTime());
		});
		try {
			config.save(new File(playerFolder, sammelPlayer.getUniqueId().toString()) + ".yml");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public long getRespawnTime() {
		if(Objects.isNull(this.pickupHandler)) return -1L;
		return this.pickupHandler.getRespawnTime();
	}
	
	public SammelPickupHandler getPickupHandler() {
		return this.pickupHandler;
	}
	
	public boolean canRespawnTime(SammelPlayer player, UUID sammelUUID) {
		if(getRespawnTime() > -1L) {
			if(player.hasCollected(sammelUUID)) {
				CollectedItem item = player.getCollectedItem(sammelUUID);
				if(Objects.nonNull(item)) {
					return canRespawnTime(item.getCollectedTime());
				}
			}
		}
		return false;
	}
	
	public String respawnText(long pickUptime) {
		pickUptime = calculateRespawn(pickUptime);
		long totalSec= (pickUptime / 1000L);
		long seconds = (pickUptime / 1000L) % 60L;
		long minutes = (pickUptime / (1000L*60L)) % 60L;
		long hours = ((pickUptime/(1000L*60L*60L)));                      

		String HumanTime= (hours+": " +minutes+ ": "+ seconds);
		return (HumanTime);
	}
	
	public boolean canRespawnTime(long pickUptime) {
		if(getRespawnTime() < 0L) {return false;}
		if(pickUptime < 0L) {return false;}
		return calculateRespawn(pickUptime) > getRespawnTime();
	}
	
	public long calculateRespawn(long pickUptime) {
		//185459831
		return System.currentTimeMillis() - pickUptime;
	}
	
	public int getAmountReal(SammelPlayer sammelPlayer) {
		return (int) sammelObjects.stream().filter(entry -> sammelPlayer.hasCollected(entry.getItemStackUUID())).count();
	}
	
	public List<SammelItem> getCollectedItems(SammelPlayer sammelPlayer){
		return sammelObjects.stream().filter(entry -> sammelPlayer.hasCollected(entry.getItemStackUUID())).collect(Collectors.toList());
	}
}

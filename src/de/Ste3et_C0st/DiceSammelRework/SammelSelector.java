package de.Ste3et_C0st.DiceSammelRework;

import org.bukkit.entity.Player;

public class SammelSelector {

	private final Player player;
	private final SammelObjectID objectID;
	
	public SammelSelector(Player player, SammelObjectID objectID) {
		this.player = player;
		this.objectID = objectID;
	}

	public Player getPlayer() {
		return player;
	}

	public SammelObjectID getObjectID() {
		return objectID;
	}
}

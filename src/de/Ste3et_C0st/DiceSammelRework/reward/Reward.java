package de.Ste3et_C0st.DiceSammelRework.reward;

import java.awt.TrayIcon.MessageType;
import java.util.Objects;

import org.bukkit.Sound;
import org.bukkit.entity.Player;

import de.Ste3et_C0st.DiceSammelRework.SammelObjectID;
import de.Ste3et_C0st.DiceSammelRework.SammelPlayer;
import de.Ste3et_C0st.DiceSammelRework.utilities.StringTranslater;
import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.chat.ComponentBuilder;

public class Reward {

	private String command, pickupMessage, name;
	private Sound sound = null;
	private int selector = 1;
	private ChatMessageType messageType = ChatMessageType.CHAT;
	private boolean all = false;
	
	public Reward(String name) {
		this.name = name;
	}
	
	public String getCommand() {
		return command;
	}
	
	public void setCommand(String command) {
		this.command = command;
	}
	
	public Sound getSound() {
		return sound;
	}
	
	public void setSound(String sound) {
		if(sound.isEmpty()) return;
		this.setSound(Sound.valueOf(sound.toUpperCase()));
	}
	
	public void setSound(Sound sound) {
		this.sound = sound;
	}
	
	public int getSelector() {
		return selector;
	}
	
	public void setSelector(int selector) {
		this.selector = selector;
	}
	
	public boolean isAll() {
		return all;
	}
	
	public void setAll(boolean all) {
		this.all = all;
	}
	public String getPickupMessage() {
		return pickupMessage;
	}
	
	public void setPickupMessage(String pickupMessage) {
		this.pickupMessage = pickupMessage;
	}

	public String getName() {
		return name;
	}
	
	public void sendMessage(Player player, StringTranslater ... translater) {
		if(getPickupMessage().isEmpty()) return;
		String message = getPickupMessage();
		
		for(StringTranslater trans : translater) {
			if(trans.getkey() != null && trans.getValue() != null) {
				message = message.replaceAll(trans.getkey(), trans.getValue());
			}
		}
		
		if(getMessageType() == ChatMessageType.ACTION_BAR) {
			player.spigot().sendMessage(getMessageType(), new ComponentBuilder(message).create());
		}else {
			player.sendMessage(message);
		}
	}

	public ChatMessageType getMessageType() {
		return messageType;
	}

	public void setMessageType(ChatMessageType messageType) {
		this.messageType = messageType;
	}
	
	public void setMessageType(String str) {
		if(str.isEmpty()) return;
		try {
			ChatMessageType messageType = ChatMessageType.valueOf(str);
			if(Objects.isNull(messageType)) return;
			this.setMessageType(messageType);
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public boolean canExecute(SammelObjectID objectID, SammelPlayer sammelPlayer) {
		int maxAmount = objectID.getSammelSet().size();
		int amountPlayer = objectID.getAmount(sammelPlayer) + 1;
		if(isAll() && maxAmount == amountPlayer) {
			return true;
		}
		
		if(getSelector() == -1) return false;
		if(getSelector() == 1) {
			return true;
		}else if((amountPlayer % getSelector()) == 0) {
			return true;
		}
		return false;
	}
}

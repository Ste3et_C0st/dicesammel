package de.Ste3et_C0st.DiceSammelRework.reward;

import java.io.File;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

import de.Ste3et_C0st.DiceSammelRework.CollectedItem;
import de.Ste3et_C0st.DiceSammelRework.SammelItem;
import de.Ste3et_C0st.DiceSammelRework.SammelObjectID;
import de.Ste3et_C0st.DiceSammelRework.SammelPlayer;
import de.Ste3et_C0st.DiceSammelRework.events.SammelCollectEvent;
import de.Ste3et_C0st.DiceSammelRework.thread.SammelPickupHandler;
import de.Ste3et_C0st.DiceSammelRework.utilities.StringTranslater;

public class FileCollector implements SammelPickupHandler{

	private PickupConfig pickupConfig;
	
	public FileCollector(File file) {
		this.pickupConfig = new PickupConfig(file);
	}
	
	@Override
	public void onPickup(SammelPlayer player, SammelCollectEvent collectEvent) {
		if(Objects.nonNull(pickupConfig)) {
			Player bukkitPlayer = player.getPlayer();
			if(pickupConfig.isPickup()) {
				if(player.getPlayer().getGameMode() != GameMode.SURVIVAL) {
					collectEvent.setCancelled(true);
					return;
				}
				
				boolean add = bukkitPlayer.getInventory().addItem(collectEvent.getSammelItem().getItemStack().clone()).isEmpty();
				if(!add) {
					collectEvent.setCancelled(true);
					return;
				}else {
					bukkitPlayer.playSound(bukkitPlayer.getLocation(), Sound.ENTITY_ITEM_PICKUP, 1, 1);
				}
			}
			
			SammelObjectID objectID = collectEvent.getObjectID();
			int amount = objectID.getAmountReal(player) + 1;
			int maxItems = objectID.getSammelSet().size();
			ConsoleCommandSender consoleSender = Bukkit.getConsoleSender();
			
			Location location = collectEvent.getSammelItem().getLocation();
			pickupConfig.getRewardSet().stream().filter(reward -> reward.canExecute(objectID, player)).forEach(reward -> {
				if(!reward.getPickupMessage().isEmpty()) {
					reward.sendMessage(player.getPlayer(), 
							new StringTranslater("%AMOUNT%", amount + ""),
							new StringTranslater("%SIZE%", maxItems + ""),
							new StringTranslater("%PLAYERNAME%", bukkitPlayer.getDisplayName()),
							new StringTranslater("%PLAYER%", bukkitPlayer.getName()),
							new StringTranslater("%WORLD%", bukkitPlayer.getWorld().getName())
					);
				}
				if(!reward.getCommand().isEmpty()) {
					Bukkit.dispatchCommand(consoleSender, reward.getCommand().replace("%PLAYER%", bukkitPlayer.getName()));
				}
				if(Objects.nonNull(reward.getSound())) {
					bukkitPlayer.playSound(location, reward.getSound(), 1, 1);
				}
			});
			if(pickupConfig.isHideCollected()) {
				collectEvent.getSammelItem().destroy(bukkitPlayer);
			}
			CollectedItem collectedItem = new CollectedItem(collectEvent.getSammelItem().getItemStackUUID(), collectEvent.getSammelItem());
			player.getCollectedUUIDs().add(collectedItem);
			
			if(Objects.nonNull(player.getLatestCollectedItem())) {
				if(collectedItem.equals(player.getLatestCollectedItem())) player.setLatestCollectedItem(null);
			}
			
			if(pickupConfig.isNavigation()) {
				Predicate<SammelItem> predicate = Objects::nonNull;
				predicate = predicate.and(entry -> !entry.getItemStackUUID().equals(collectEvent.getSammelItem().getItemStackUUID()));
				predicate = predicate.and(entry -> player.canCollect(entry, objectID));
				List<SammelItem> itemList = objectID.sammelObjects.stream().filter(predicate).collect(Collectors.toList());
				if(!itemList.isEmpty()) {
					Collections.shuffle(itemList);
					SammelItem item = itemList.stream().findFirst().orElse(null);
					if(Objects.nonNull(item)) {
						bukkitPlayer.setCompassTarget(item.getLocation());
					}
				}
			}
		}
	}

	public PickupConfig getPickupConfig() {
		return pickupConfig;
	}

	@Override
	public boolean isHideCollected() {
		return this.pickupConfig.isHideCollected();
	}

	@Override
	public long getRespawnTime() {
		return this.pickupConfig.getRespawnTime();
	}

	@Override
	public boolean isNavigation() {
		return this.pickupConfig.isNavigation();
	}
}

package de.Ste3et_C0st.DiceSammelRework.reward;

import java.io.File;
import java.io.InputStreamReader;
import java.util.HashSet;

import org.bukkit.configuration.file.YamlConfiguration;

import de.Ste3et_C0st.DiceSammelRework.DiceSammelMain;
import net.md_5.bungee.api.ChatColor;

public class PickupConfig {

	private boolean pickup = false, hideCollected = false, compass = false;
	private long respawnTime = -1L;
	private HashSet<Reward> rewardSet = new HashSet<Reward>();
	
	public PickupConfig(File file) {
		if(!file.getParentFile().exists()) file.getParentFile().mkdirs();
		if(file.exists()) {
			this.load(YamlConfiguration.loadConfiguration(file));
		}else {
			try(InputStreamReader reader = new InputStreamReader(DiceSammelMain.getInstance().getResource("pickupConfig.yml"))){
				YamlConfiguration configuration = YamlConfiguration.loadConfiguration(reader);
				configuration.options().copyHeader(true);
				configuration.save(file);
				this.load(configuration);
			}catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	public PickupConfig(YamlConfiguration config) {
		this.load(config);
	}
	
	private void load(YamlConfiguration config) {
		this.pickup = config.getBoolean("config.pickup", false);
		this.hideCollected = config.getBoolean("config.hideCollected", false);
		this.compass =  config.getBoolean("config.compass", false);
		this.respawnTime = config.getLong("config.respawnTime", -1L);
		if(this.respawnTime < 100) this.respawnTime = -1L;
		if(config.isConfigurationSection("config.rewards")) {
			config.getConfigurationSection("config.rewards").getKeys(false).stream().forEach(key -> {
				String header = "config.rewards." + key;
				Reward reward = new Reward(key);
				reward.setCommand(config.getString(header + ".command", ""));
				reward.setSound(config.getString(header + ".sound", ""));
				reward.setPickupMessage(ChatColor.translateAlternateColorCodes('&', config.getString(header + ".message.string", "")));
				reward.setMessageType(config.getString(header + ".message.type", "CHAT"));
				int selector = config.getInt(header + ".selector", 0);
				if(selector == -1) {
					reward.setAll(true);
					reward.setSelector(-1);
				}else {
					reward.setSelector(selector);
				}
				rewardSet.add(reward);
			});
		}
	}

	public boolean isPickup() {
		return this.pickup;
	}
	
	public boolean isNavigation() {
		return this.compass;
	}

	public void setPickup(boolean pickup) {
		this.pickup = pickup;
	}

	public HashSet<Reward> getRewardSet() {
		return this.rewardSet;
	}
	
	public boolean isHideCollected() {
		return this.hideCollected;
	}
	
	public long getRespawnTime() {
		return this.respawnTime;
	}
	
}

package de.Ste3et_C0st.DiceSammelRework;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.UUID;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.ProtocolManager;
import com.comphenix.protocol.events.PacketContainer;
import com.comphenix.protocol.wrappers.WrappedDataWatcher;
import com.comphenix.protocol.wrappers.WrappedDataWatcher.Registry;
import com.comphenix.protocol.wrappers.WrappedDataWatcher.WrappedDataWatcherObject;

public class ArmorStandPacket {

	
	private WrappedDataWatcher metadata = new WrappedDataWatcher();
	private boolean visible = false, small = false;
	private final Location location;
	private final Integer entityID = EntityID.nextEntityId();
	private final UUID uuid;
	private final PacketContainer entityContainer = new PacketContainer(PacketType.Play.Server.SPAWN_ENTITY_LIVING);
	private final PacketContainer pessangerContainer = new PacketContainer(PacketType.Play.Server.MOUNT);
	private final PacketContainer metadataContainer = new PacketContainer(PacketType.Play.Server.ENTITY_METADATA);
	private static ProtocolManager protocolManager = ProtocolLibrary.getProtocolManager();
	private List<Integer> passengerIDs = new ArrayList<>();
	
	public ArmorStandPacket(Location location) {
		this.uuid = UUID.randomUUID();
		this.location = location;
		
		getHandle().getModifier().writeDefaults();
		getHandle().getIntegers().write(0, this.entityID).write(1, 1);
		getHandle().getUUIDs().write(0, getUuid());
		
		this.metadataContainer.getIntegers().write(0, this.entityID);
		this.pessangerContainer.getIntegers().write(0, this.entityID);
		this.setLocation(getLocation());
		this.setSmall(true);
	}
	
	public int getEntityID() {
		return this.entityID;
	}

	public Location getLocation() {
		return location;
	}

	public UUID getUuid() {
		return uuid;
	}
	
	public PacketContainer getHandle() {
		return entityContainer;
	}
	
	public void send(final Collection<Player> playerCollection) {
		playerCollection.stream().forEach(this::send);
	}
	
	public void send(final Player ... players) {
		Arrays.asList(players).stream().forEach(this::send);
	}
	
	public void destroy(final Collection<Player> playerCollection) {
		playerCollection.stream().forEach(this::destroy);
	}
	
	public void destroy(final Player ... players) {
		Arrays.asList(players).stream().forEach(this::destroy);
	}
	
	public ArmorStandPacket setInvisible(boolean b) {
        setBitMask(b, 0, 5);
        this.visible = !b;
        return this;
    }
	
	public ArmorStandPacket setSmall(boolean b) {
<<<<<<< HEAD
        setBitMask(b, 15, 0);
        this.visible = !b;
        return this;
    }
	
	public boolean isInvisible() {
		return !this.visible;
	}
	
	public boolean isSmall() {
		return this.small;
	}
	
	public void destroy(final Player player) {
		PacketContainer destroy = new PacketContainer(PacketType.Play.Server.ENTITY_DESTROY);
		destroy.getIntegerArrays().writeSafely(0, new int[] {getEntityID()});
		destroy.getIntegers().writeSafely(0, getEntityID());
		destroy.getIntLists().writeSafely(0, Arrays.asList(getEntityID()));
=======
        setBitMask(b, 14, 0);
        this.visible = !b;
        return this;
    }
	
	public boolean isInvisible() {
		return !this.visible;
	}
	
	public boolean isSmall() {
		return this.small;
	}
	
	public void destroy(final Player player) {
		PacketContainer destroy = new PacketContainer(PacketType.Play.Server.ENTITY_DESTROY);
		destroy.getIntegerArrays().write(0, new int[] {getEntityID()});
>>>>>>> branch 'master' of https://gitlab.com/Ste3et_C0st/dicesammel.git
		try {
			protocolManager.sendServerPacket(player, destroy);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void sendMount(Player ... players) {
		Arrays.asList(players).stream().forEach(this::sendMount);
	}
	
	public void sendMount(Player player) {
		pessangerContainer.getIntegerArrays().write(0, this.passengerIDs.stream().mapToInt(Integer::intValue).toArray());
		try {
			protocolManager.sendServerPacket(player, pessangerContainer);
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void send(final Player player) {
		try {
			this.metadataContainer.getWatchableCollectionModifier().write(0, this.metadata.getWatchableObjects());
			protocolManager.sendServerPacket(player, this.getHandle());
			protocolManager.sendServerPacket(player, this.metadataContainer);
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public List<Integer> getPassenger() {
        return this.passengerIDs;
    }
	
	private void setLocation(final Location location) {
		getHandle().getDoubles().write(0, location.getX()).write(1, location.getY() + .2).write(2, location.getZ());
	}
	
	public void setBitMask(boolean flag, int field, int i) {
        byte b0 = (byte) 0;
        if (metadata.hasIndex(field)) {
            b0 = (byte) metadata.getObject(new WrappedDataWatcherObject(field, Registry.get(Byte.class)));
        }
        if (flag) {
        	metadata.setObject(new WrappedDataWatcherObject(field, Registry.get(Byte.class)), (byte) (b0 | 1 << i));
        } else {
        	metadata.setObject(new WrappedDataWatcherObject(field, Registry.get(Byte.class)), (byte) (b0 & ~(1 << i)));
        }
    }
	
}
